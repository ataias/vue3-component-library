import vue from "@vitejs/plugin-vue";
import { defineConfig } from "vite";

import path from "path";
// import typescript2 from "rollup-plugin-typescript2";
import cssInjectedByJsPlugin from "vite-plugin-css-injected-by-js";
import dts from "vite-plugin-dts";
import tsconfigPaths from "vite-tsconfig-paths";

const __dirname = path.dirname(new URL(import.meta.url).pathname);

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    dts({ copyDtsFiles: true, outDir: ['dist'], insertTypesEntry: true, compilerOptions: {
      declarationMap: true
    } }),
    cssInjectedByJsPlugin(),
    // typescript2({
    //   check: false,
    //   include: ["src/components/**/*.vue"],
    //   tsconfigOverride: {
    //     compilerOptions: {
    //       outDir: "dist",
    //       sourceMap: true,
    //       declaration: true,
    //       declarationMap: true,
    //     },
    //   },
    //   exclude: ["vite.config.ts"],
    // }),
    tsconfigPaths(),
  ],
  build: {
    cssCodeSplit: true,
    lib: {
      // Could also be a dictionary or array of multiple entry points
      entry: "src/components/index.ts",
      name: "vue3ComponentLibrary",
      formats: ["es", "cjs", "umd"],
      fileName: (format) => `vue3-component-library.${format}.js`,
    },
    rollupOptions: {
      // make sure to externalize deps that should not be bundled
      // into your library
      input: {
        main: path.resolve(__dirname, "src/components/main.ts"),
      },
      external: ["vue"],
      output: {
        assetFileNames: (assetInfo) => {
          if (assetInfo.name === "main.css") return "my-library-vue-ts.css";
          return assetInfo.name;
        },
        exports: "named",
        globals: {
          vue: "Vue",
        },
      },
    },
  },
});
