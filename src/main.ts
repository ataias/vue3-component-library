import { HelloWorld, MyButton } from "@/components";
import type { App } from "vue";

export default {
  install: (app: App) => {
    app.component("HelloWorld", HelloWorld);
    app.component("MyButton", MyButton);
  },
};

export { HelloWorld, MyButton };
