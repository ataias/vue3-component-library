#!/usr/bin/env bash

# This file needs to be run once after clone and also after file changes for
# devs. For CI, this file does not need to run necessarily, unless you want to
# output a nice diff for the lockb files.

git config diff.lockb.textconv bun
git config diff.lockb.binary true